import axios from 'axios'
import { Toast } from 'vant'
// import store from '@/store'
// import { getToken } from '@/utils/auth'
// import { hideLoading, showLoading } from './loading'

// 请求超时时间, 单位:毫秒
const requestTimeout = 30000
let requestLoading = null

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: requestTimeout // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    showLoading()
    // if (!config.noLoading) {}

    // if (store.getters.token) {
    // let each request carry token
    // ['X-Token'] is a custom headers key
    // please modify it according to the actual situation
    //   config.headers['X-Token'] = getToken()
    // }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

  /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
  response => {
    hideLoading()
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 0) {
      // error
      alert(res.msg)

      console.log('resp:' + JSON.stringify(res))

      // 401: Illegal token | Other clients logged in | Token expired;
      if (res.code === 401) {
        // to re-login
        alert('登录状态已过期')
      }
      return Promise.reject(res.message || 'error')
    } else {
      return res
    }
  },
  error => {
    hideLoading()
    console.log('[error]: ' + error) // for debug
    let msg = error.message
    if (msg === 'Network Error') {
      msg = '网络错误或服务器忙碌'
    }
    if (msg === 'timeout of 10000ms exceeded') {
      msg = '请求超时,请稍候再试'
    }
    alert(msg)
    return Promise.reject(error)
  }
)

function showLoading() {
  requestLoading = Toast.loading({
    message: '加载中...',
    forbidClick: true,
    loadingType: 'spinner',
    duration: 0
  })
}

function hideLoading() {
  if (requestLoading) {
    requestLoading.clear()
  }
}

export default service
