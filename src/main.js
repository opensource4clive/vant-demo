import Vue from 'vue'
import App from './App'
import { router } from './router'
import { } from './vant-components'

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})
