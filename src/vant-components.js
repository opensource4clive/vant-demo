import Vue from 'vue'

import {
  Toast,
  Tag,
  Col,
  Icon,
  Cell,
  CellGroup,
  Checkbox,
  CheckboxGroup
} from 'vant'

Vue.use(Toast)
  .use(Tag)
  .use(Col)
  .use(Icon)
  .use(Cell)
  .use(CellGroup)
  .use(Checkbox)
  .use(CheckboxGroup)

