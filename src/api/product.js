import request from '@/utils/request'

export function detail(id) {
  return request({
    noLoading: true,
    url: `/product/${id}`,
    method: 'get'
  })
}
