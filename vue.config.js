// dev port
const port = 9528

module.exports = {
  outputDir: 'dist',
  publicPath: '/',
  lintOnSave: process.env.NODE_ENV === 'development',
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: getProxy(),
    after: getAfter()
  }
}

function getAfter() {
  if (process.env.NODE_ENV === 'development') {
    return () => {}
  }
  return require('./mock/mock-server.js')
}

function getProxy() {
  if (process.env.NODE_ENV === 'development') {
    return false
  }
  return {
    [process.env.VUE_APP_BASE_API]: {
      target: `http://localhost:${port}/mock`,
      changeOrigin: true,
      pathRewrite: {
        ['^' + process.env.VUE_APP_BASE_API]: ''
      }
    }
  }
}

