// import Mock from 'mockjs'

// mock api
export default [

  // get
  {
    url: '/product/[0-9]',
    type: 'get',
    response: config => {
      return {
        code: 0,
        data: {
          title: '22美国伽力果（约680g/3个）',
          price: 2680,
          express: '免运费',
          remain: 19,
          thumb: [
            'https://img.yzcdn.cn/public_files/2017/10/24/e5a5a02309a41f9f5def56684808d9ae.jpeg',
            'https://img.yzcdn.cn/public_files/2017/10/24/1791ba14088f9c2be8c610d0a6cc0f93.jpeg'
          ]
        }
      }
    }
  }

]

